let importCoreJS = document.createElement('script');
importCoreJS.src = 'core.js';
document.head.appendChild(importCoreJS);

let table = 'Events';

function deleteEvent(eventID)
{
    let data = {
        id: eventID,
        table: table,
    };

    fetch(requestFile, {
        method: "DELETE",
        body: JSON.stringify(data),
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
        },

    })
        .then( (response) => response.json() ) // Transform the data into json
        .then( (data) => {
            alert("Event deleted!");
            getEvents();
        })
        .catch( (error) => {
            console.log(error);
            alert("Something went wrong! Check the development console for more details.");
        });
}

function getEvents()
{
    let query = 'getAll';
    let queryURL = location.href;
    queryURL = queryURL.replace('events.html', 'Requests.php');
    queryURL = addParameterToURL('table=' + table, queryURL);
    queryURL = addParameterToURL('query=' + query, queryURL);

    fetch(queryURL)
        .then( (response) => response.json() )
        .then( (data) => {
            let tableHeader = `<table align="center">
                <thead>
                    <tr>
                        <td>Event</td>
                        <td>Date</td>
                        <td>Actions</td>
                    </tr>
                </thead>
                <tbody>`;
            let tableFooter = `</tbody>
                </table>
                </div>`;
            drawTable(data, tableHeader, tableFooter, table, 'eventsTable');
        })
}

function addEvent()
{
    let event = document.getElementById('event').value;
    let eventDate = document.getElementById('eventDate').value;
    if(event.trim() === "")
    {
        alert("Please fill in what the event is!");
        return;
    }

    let data = {
        table: table,
        event: event,
        date: eventDate,
    };

    fetch(requestFile, {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
        },

    })
        .then( (response) => response.json() ) // Transform the data into json
        .then( (data) => {
            alert("Event added!");
            document.getElementById('event').value = '';
            setTodayDate('events.html');
            getEvents();

        })
        .catch( (error) => {
            console.log(error);
            alert("Something went wrong! Check the development console for more details.");
        });
}
