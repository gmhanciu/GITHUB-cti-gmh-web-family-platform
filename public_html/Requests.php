<?php

require_once 'Helpers.php';
require_once 'DBConnection.php';

switch ($_SERVER['REQUEST_METHOD'])
{
    case 'GET':
        try
        {
            $data = (object) $_GET;
            $tableName = $data->table;
            unset($data->table);
            switch ($data->query)
            {
                case 'getAll':
                    $sqlQuery = "SELECT * FROM $tableName";
                    $preparedSqlQuery = $pdo->prepare($sqlQuery);
                    $preparedSqlQuery->execute();
                    $response = $preparedSqlQuery->fetchAll();
                    break;
                case 'getChores':
                    $sqlQuery = "SELECT $tableName.*, FamilyMembers.name as 'assignedTo' FROM $tableName LEFT JOIN FamilyMembers ON FamilyMembers.id = Chores.assignedTo";
                    $preparedSqlQuery = $pdo->prepare($sqlQuery);
                    $preparedSqlQuery->execute();
                    $response = $preparedSqlQuery->fetchAll();
                    break;
            }
        }
        catch (\Exception $e)
        {
            $response = $e->getMessage();
        }
        break;
    case 'PUT':
        try
        {
            $requestBody = file_get_contents('php://input');
            $data = json_decode($requestBody);
            $tableName = $data->table;
            unset($data->table);
            $sqlQuery = "UPDATE `$tableName` SET status = 1 WHERE ";
            foreach ($data as $entry => $value)
            {
                $sqlQuery .= "$entry = :$entry";

            }
            $preparedSqlQuery = $pdo->prepare($sqlQuery);
            foreach ($data as $entry => $value)
            {
                $preparedSqlQuery->bindParam(":$entry", $data->$entry);
            }
            $preparedSqlQuery->execute();
            $response = 'success';
        }
        catch (\Exception $e)
        {
            $response = $e->getMessage();
        }
        break;
    case 'POST':
        try
        {
            $requestBody = file_get_contents('php://input');
            $data = json_decode($requestBody);
            $tableName = $data->table;
            unset($data->table);
            $sqlQuery = "INSERT into `$tableName` (";
            foreach ($data as $entry => $value)
            {
                $sqlQuery .= "`$entry`, ";
            }
            $sqlQuery = substr($sqlQuery, 0, strlen($sqlQuery) - 2);
            $sqlQuery .= ') VALUES (';
            foreach ($data as $entry => $value)
            {
                $sqlQuery .= ":$entry, ";
            }
            $sqlQuery = substr($sqlQuery, 0, strlen($sqlQuery) - 2);
            $sqlQuery .= ")";
            $preparedSqlQuery = $pdo->prepare($sqlQuery);
            foreach ($data as $entry => $value)
            {
                $preparedSqlQuery->bindParam(":$entry", $data->$entry);
            }
            $preparedSqlQuery->execute();
            $response = 'success';
        }
        catch (\Exception $e)
        {
            $response = $e->getMessage();
        }
        break;
    case "DELETE":
        try
        {
            $requestBody = file_get_contents('php://input');
            $data = json_decode($requestBody);
            $tableName = $data->table;
            unset($data->table);
            $sqlQuery = "DELETE FROM `$tableName` WHERE ";
            foreach ($data as $entry => $value)
            {
                $sqlQuery .= "$entry = :$entry";

            }
            $preparedSqlQuery = $pdo->prepare($sqlQuery);
            foreach ($data as $entry => $value)
            {
                $preparedSqlQuery->bindParam(":$entry", $data->$entry);
            }
            $preparedSqlQuery->execute();
            $response = 'success';
        }
        catch (\Exception $e)
        {
            $response = $e->getMessage();
        }
        break;
}


echo json_encode($response);