let importCoreJS = document.createElement('script');
importCoreJS.src = 'core.js';
document.head.appendChild(importCoreJS);

let table = 'ShoppingList';

function deleteFromShoppingList(toDeleteID)
{
    let data = {
        id: toDeleteID,
        table: table,
    };

    fetch(requestFile, {
        method: "DELETE",
        body: JSON.stringify(data),
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
        },

    })
        .then( (response) => response.json() ) // Transform the data into json
        .then( (data) => {
            alert("Entry deleted!");
            getShoppingList();
        })
        .catch( (error) => {
            console.log(error);
            alert("Something went wrong! Check the development console for more details.");
        });
}

function markAsBought(toBuyID)
{
    let data = {
        id: toBuyID,
        table: table,
    };

    fetch(requestFile, {
        method: "put",
        body: JSON.stringify(data),
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
        },
    })
        .then( (response) => response.json() ) // Transform the data into json
        .then( (data) => {
            alert("Entry status updated!");
            getShoppingList();
        })
        .catch( (error) => {
            console.log(error);
            alert("Something went wrong! Check the development console for more details.");
        });
}

function getShoppingList()
{
    let query = 'getAll';
    let queryURL = location.href;
    queryURL = queryURL.replace('shopping.html', 'Requests.php');
    queryURL = addParameterToURL('table=' + table, queryURL);
    queryURL = addParameterToURL('query=' + query, queryURL);

    fetch(queryURL)
        .then( (response) => response.json() )
        .then( (data) => {
            let tableHeader = `<table align="center">
                <thead>
                    <tr>
                        <td>To Buy</td>
                        <td>Status</td>
                        <td>Actions</td>
                    </tr>
                </thead>
                <tbody>`;
            let tableFooter = `</tbody>
                </table>
                </div>`;
            drawTable(data, tableHeader, tableFooter, table, 'shoppingListTable');
        })
}

function addToShoppingList()
{
    let toBuy = document.getElementById('toBuy').value;
    if(toBuy.trim() === "")
    {
        alert("Please fill in what you want to add to the shopping list!");
        return;
    }

    let data = {
        table: table,
        name: toBuy,
    };

    fetch(requestFile, {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
        },

    })
        .then( (response) => response.json() ) // Transform the data into json
        .then( (data) => {
            alert("Entry has been added to the shopping list!");
            document.getElementById('toBuy').value = '';
            getShoppingList();

        })
        .catch( (error) => {
            console.log(error);
            alert("Something went wrong! Check the development console for more details.");
        });
}