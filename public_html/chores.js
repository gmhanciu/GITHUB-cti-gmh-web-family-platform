let importCoreJS = document.createElement('script');
importCoreJS.src = 'core.js';
document.head.appendChild(importCoreJS);

let table = 'Chores';

function deleteChore(choreID)
{
    // table = 'Chores';

    let data = {
        id: choreID,
        table: table,
    };

    fetch(requestFile, {
        method: "DELETE",
        body: JSON.stringify(data),
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
        },

    })
        .then( (response) => response.json() ) // Transform the data into json
        .then( (data) => {
            alert("Chore deleted!");
            getChores();
        })
        .catch( (error) => {
            console.log(error);
            alert("Something went wrong! Check the development console for more details.");
        });
}

function completeChore(choreID)
{
    // table = 'Chores';
    let data = {
        id: choreID,
        table: table,
    };

    fetch(requestFile, {
        method: "put",
        body: JSON.stringify(data),
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
        },
    })
        .then( (response) => response.json() ) // Transform the data into json
        .then( (data) => {
            alert("Chore status updated ! ");
            getChores();
        })
        .catch( (error) => {
            console.log(error);
            alert("Something went wrong! Check the development console for more details.");
        });
}

function getChores()
{
    // table = 'Chores';
    let query = 'getChores';
    let queryURL = location.href;
    queryURL = queryURL.replace('chores.html', 'Requests.php');
    queryURL = addParameterToURL('table=' + table, queryURL);
    queryURL = addParameterToURL('query=' + query, queryURL);

    fetch(queryURL)
        .then( (response) => response.json() )
        .then( (data) => {
            let tableHeader = `<table align="center">
                <thead>
                    <tr>
                        <td>Chore</td>
                        <td>Assigned To</td>
                        <td>Status</td>
                        <td>Actions</td>
                    </tr>
                </thead>
                <tbody>`;
            let tableFooter = `</tbody>
                </table>
                </div>`;
            drawTable(data, tableHeader, tableFooter, table, 'choresTable');
        })
}

function addChore()
{
    let chore = document.getElementById('chore').value;
    let choreAssigneeSelect = document.getElementById('assignedTo');
    let assignedTo = choreAssigneeSelect.options[choreAssigneeSelect.selectedIndex].value;
    // table = 'Chores';
    if(chore.trim() === "")
    {
        alert("Please fill in what the chore is!");
        return;
    }

    let data = {
        table: table,
        chore: chore,
        assignedTo: assignedTo,
    };

    fetch(requestFile, {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
        },

    })
        .then( (response) => response.json() ) // Transform the data into json
        .then( (data) => {
            alert("Chore has been added!");
            document.getElementById('chore').value = '';
            choreAssigneeSelect.value = 1;
            getChores();

        })
        .catch( (error) => {
            console.log(error);
            alert("Something went wrong! Check the development console for more details.");
        });
}