let requestFile = 'Requests.php';

function drawTable(data, tableHeader, tableFooter, table, table_id)
{
    let header = tableHeader;
    let footer = tableFooter;
    let content = '';
    switch (table)
    {
        case 'Chores':
            for (let entry in data)
            {
                content += `<tr>
                    <td>${data[entry].chore}</td>
                    <td>${data[entry].assignedTo}</td>`;
                if (data[entry].status === 1)
                {
                    content += `<td>Completed</td>`
                    content += `<td><button class="button button-secondary" disabled onclick="completeChore(${data[entry].id})">Complete</button><button class="button button-danger" onclick="deleteChore(${data[entry].id})">Delete</button></td></tr>`;
                }
                else
                {
                    content += `<td>Not Completed</td>`
                    content += `<td><button class="button button-success" onclick="completeChore(${data[entry].id})">Complete</button><button class="button button-danger" onclick="deleteChore(${data[entry].id})">Delete</button></td></tr>`;
                }
            }
            document.getElementById(table_id).innerHTML = header + content + footer;
            break;
        case 'ShoppingList':
            for (let entry in data)
            {
                content += `<tr>
                    <td>${data[entry].name}</td>`;
                if (data[entry].status === 1)
                {
                    content += `<td>Bought</td>`
                    content += `<td><button class="button button-secondary" disabled onclick="markAsBought(${data[entry].id})">Mark As Bought</button><button class="button button-danger" onclick="deleteFromShoppingList(${data[entry].id})">Delete</button></td></tr>`;
                }
                else
                {
                    content += `<td>Not Bought</td>`
                    content += `<td><button class="button button-success" onclick="markAsBought(${data[entry].id})">Mark As Bought</button><button class="button button-danger" onclick="deleteFromShoppingList(${data[entry].id})">Delete</button></td></tr>`;
                }
            }
            document.getElementById(table_id).innerHTML = header + content + footer;
            break;
        case 'Events':
            for (let entry in data)
            {
                content += `<tr>
                    <td>${data[entry].event}</td>
                    <td>${data[entry].date}</td>
                    <td><button class="button button-danger" onclick="deleteEvent(${data[entry].id})">Delete</button></td></tr>`;
            }
            document.getElementById(table_id).innerHTML = header + content + footer;
            break;
    }

}

function addParameterToURL(param, URL){
    let _url = URL;
    _url += (_url.split('?')[1] ? '&':'?') + param;
    return _url;
}

function callOnLoad(page)
{
    switch (page)
    {
        case 'chores.html':
            addMembersAsOptions(page);
            getChores();
            break;
        case 'shopping.html':
            getShoppingList();
            break;
        case 'events.html':
            setTodayDate(page);
            getEvents();
            break;
    }
}

function setTodayDate(page)
{
    let dateInputID = '';
    switch (page)
    {
        case 'events.html':
            dateInputID = 'eventDate';
            break;
    }
    let dateInput = document.getElementById(dateInputID);
    let today = new Date().toISOString().split('T')[0];
    dateInput.value = today;
    dateInput.setAttribute("min", today);
}

function addMembersAsOptions(page)
{
    let table = 'FamilyMembers';
    let query = 'getAll';
    let queryURL = location.href;
    queryURL = queryURL.replace(page, 'Requests.php');
    queryURL = addParameterToURL('table=' + table, queryURL);
    queryURL = addParameterToURL('query=' + query, queryURL);

    fetch(queryURL, {
        method: "get",
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
        },

    })
        .then( (response) => response.json() ) // Transform the data into json
        .then( (data) => {
            for (let familyMember of data)
            {
                let option = document.createElement('option');
                option.text = familyMember.name;
                option.value = familyMember.id;
                document.getElementById('assignedTo').appendChild(option);
            }
        })
        .catch( (error) => {
            console.log(error);
            alert("Something went wrong! Check the development console for more details.");
        });
}

