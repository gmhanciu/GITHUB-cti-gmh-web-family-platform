<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);



$options = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
];

$host = 'WEBGMH.DEVELOP.EIDDEW.COM';
$db   = 'web';
//$charset = 'utf8mb4';
//;charset=$charset
$dsn = "mysql:host=$host;dbname=$db";

try
{
    $user = 'webgmh';
    $pass = 'qwerty123456';
    $pdo = new PDO($dsn, $user, $pass, $options);
}
catch (\PDOException $e)
{
    throw new \PDOException($e->getMessage(), (int)$e->getCode());
}